#!/bin/bash
#
# Convert project in print document

set -e

readonly DIRECTORY=${1%/}

##############################
# check in arguments
# Globals:
#   DIRECTORY
# Arguments:
#   None
# Returns:
#   None
##############################
checkin() {
    if [[ ! -d $DIRECTORY ]]; then
        echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: '$DIRECTORY' is not a directory" >&2
        exit 1
    fi
}

##############################
# Retrieve if the file is text
# Globals:
#   None
# Arguments:
#   file
# Returns:
#   true if is text file, false otherwise
##############################
is_text_file() {
    local file=$1
    if [[ "$(file $file | grep 'text' | wc -l)" == "0" ]]; then
        return 1
    else
        return 0
    fi
}

##############################
# create_pdf
# Globals:
#   DIRECTORY
# Arguments:
#   None
# Returns:
#   None
##############################
create_pdf() {

    tree ${DIRECTORY} > "${DIRECTORY}.txt"
    cat "${DIRECTORY}.txt" | iconv -c -f utf-8 -t ISO-8859-1 | enscript -o "${DIRECTORY}.ps"
    ps2pdf "${DIRECTORY}.ps" "${DIRECTORY}_tree.pdf"
    rm "${DIRECTORY}.txt" "${DIRECTORY}.ps"

    local files="$(find $DIRECTORY -type f)"
    for file in ${files}; do
        if is_text_file $file; then
            local fileps="${file}.ps"
            local filepdf="${file}.pdf"
            enscript "${file}" -o "${fileps}"
            ps2pdf "${fileps}" "${filepdf}"
        fi
    done

    pdfs=""
    for i in $(find ${DIRECTORY} -name "*.pdf" -type f);
    do
        if [[ -z "$pdfs" ]]; then
            pdfs="$i"
        else
            pdfs="$pdfs $i"
        fi
    done

    pdfunite $pdfs "${DIRECTORY}.pdf"
    find ${DIRECTORY} -name "*.ps" -type f -delete
    find ${DIRECTORY} -name "*.pdf" -type f -delete

    pdfunite "${DIRECTORY}_tree.pdf" "${DIRECTORY}.pdf" "temp.pdf"
    mv "temp.pdf" "${DIRECTORY}.pdf"
    rm "${DIRECTORY}_tree.pdf"
}

##############################
# main
#
##############################
main() {
    checkin
    create_pdf
}

main
